<?php

function putPiece($piece1, $piece2){
    global $flip;
    if(!$flip){
        return "<img class= \"piece\" src = \"./images/{$piece1}.png\" alt=\"rook image\">";
    }
    else{
        return "<img class= \"piece\" src = \"./images/{$piece2}.png\" alt=\"rook image\">";
    }
}