<?php

function getColor($odd_or_even, $flip) {
    if($flip === FALSE)
    {
        if($odd_or_even === "odd")
        {
            return "white-first";
        }
        else
        {
            return "black-first";
        }
    }
    else{
        if($odd_or_even === "odd")
        {
            return "black-first";
        }
        else
        {
            return "white-first";
        }
    }
}

function getCellLabel ($flip, $id)
{
    if($flip === FALSE)
    {
        switch($id%10)
        {
            case 1:
            $columnLabel = 'a';
            break;
            case 2:
            $columnLabel = 'b';
            break;
            case 3:
            $columnLabel = 'c';
            break;
            case 4:
            $columnLabel = 'd';
            break;
            case 5:
            $columnLabel = 'e';
            break;
            case 6:
            $columnLabel = 'f';
            break;
            case 7:
            $columnLabel = 'g';
            break;
            case 8:
            $columnLabel = 'h';
            break;
        }
        $rowLabel = 9 - intdiv($id, 10);
    }
    else
    {
        $rowLabel = intdiv($id, 10);
        switch($id%10)
        {
            case 1:
            $columnLabel = 'h';
            break;
            case 2:
            $columnLabel = 'g';
            break;
            case 3:
            $columnLabel = 'f';
            break;
            case 4:
            $columnLabel = 'e';
            break;
            case 5:
            $columnLabel = 'd';
            break;
            case 6:
            $columnLabel = 'c';
            break;
            case 7:
            $columnLabel = 'b';
            break;
            case 8:
            $columnLabel = 'a';
            break;
        }
    }
    echo "<p class=\"row-address\">$rowLabel</p>";
    echo "<p class=\"col-address\">$columnLabel</p>";
}

function getRowLabel($flip, $id){
    if(!$flip){
        $rowLabel = 9 - intdiv($id, 10);
    }
    else{
        $rowLabel = intdiv($id, 10);
    }
    echo "<p class=\"row-address\">$rowLabel</p>";    
}

function getColumnLabel($flip, $id){
    if(!$flip){
        switch($id%10)
        {
            case 1:
            $columnLabel = 'a';
            break;
            case 2:
            $columnLabel = 'b';
            break;
            case 3:
            $columnLabel = 'c';
            break;
            case 4:
            $columnLabel = 'd';
            break;
            case 5:
            $columnLabel = 'e';
            break;
            case 6:
            $columnLabel = 'f';
            break;
            case 7:
            $columnLabel = 'g';
            break;
            case 8:
            $columnLabel = 'h';
            break;
        }
    }
    else{
        switch($id%10)
        {
            case 1:
            $columnLabel = 'h';
            break;
            case 2:
            $columnLabel = 'g';
            break;
            case 3:
            $columnLabel = 'f';
            break;
            case 4:
            $columnLabel = 'e';
            break;
            case 5:
            $columnLabel = 'd';
            break;
            case 6:
            $columnLabel = 'c';
            break;
            case 7:
            $columnLabel = 'b';
            break;
            case 8:
            $columnLabel = 'a';
            break;
        }
    }
    echo "<p class=\"col-address\">$columnLabel</p>";    
}
