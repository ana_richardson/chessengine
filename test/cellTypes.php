<?php

function cellType($type){
    global $flip;
    if(!$flip){
        return $type;
    }
    else{
        if($type === 'dark'){return 'white';}
        else{return 'dark';}
    }
}