<?php
include './backgroundColor.php';
include './putPieces.php';
include './cellTypes.php';
?>

<!DOCTYPE html
<html>
    <head>
        <title>Chess Engine</title>
        <link rel = "stylesheet" type = "text/css" href = "styles.css">
    </head>
    <body>
        <?php
        $flip = FALSE;#initial value

        if(count($_POST["pieceSelect"]) > 0 && $_POST["pieceSelect"][0] === "white")
        {
            $flip = false;
        }
        else
        {
            $flip = TRUE;
        }
        ?>
        <div class = "board">
            <div class = "row oddRow <?= getColor("odd", $flip);?>">
                <div class = "cell <?= cellType('white');?>" id = 11>
                    <?php getRowLabel($flip, 11);?>
                    <?=putPiece('black_rook', 'white_rook');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 12>
                    <?=putPiece('black_knight', 'white_knight');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 13>
                    <?=putPiece('black_bishop', 'white_bishop');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 14>
                    <?=putPiece('black_queen', 'white_queen');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 15>
                    <?=putPiece('black_king', 'white_king');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 16>
                    <?=putPiece('black_bishop', 'white_bishop');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 17>
                    <?=putPiece('black_knight', 'white_knight');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 18>
                    <?=putPiece('black_rook', 'white_rook');?>
                </div>
            </div>
            <div class = "row evenRow <?= getColor("even", $flip);?>">
                <div class = "cell <?= cellType('dark');?>" id = 21>
                    <?php getRowLabel($flip, 21);?>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 22>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 23>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 24>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 25>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 26>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 27>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 28>
                    <?=putPiece('black_pawn', 'white_pawn');?>
                </div>
            </div>
            <div class = "row oddRow <?= getColor("odd", $flip);?>">
                <div class = "cell <?= cellType('white');?>" id = 31>
                    <?php getRowLabel($flip, 31);?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 32>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 33>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 34>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 35>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 36>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 37>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 38>
                </div>
            </div>
            <div class = "row evenRow <?= getColor("even", $flip);?>">
                <div class = "cell <?= cellType('dark');?>" id = 41>
                    <?php getRowLabel($flip, 41);?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 42>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 43>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 44>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 45>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 46>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 47>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 48>
                </div>
            </div>
            <div class = "row oddRow <?= getColor("odd", $flip);?>">
                <div class = "cell <?= cellType('white');?>" id = 51>
                    <?php getRowLabel($flip, 51);?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 52>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 53>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 54>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 55>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 56>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 57>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 58>
                </div>
            </div>
            <div class = "row evenRow <?= getColor("even", $flip);?>">
                <div class = "cell <?= cellType('dark');?>" id = 61>
                    <?php getRowLabel($flip, 61);?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 62>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 63>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 64>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 65>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 66>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 67>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 68>
                </div>
            </div>
            <div class = "row oddRow <?= getColor("odd", $flip);?>">
                <div class = "cell <?= cellType('white');?>" id = 71>
                    <?php getRowLabel($flip, 71);?>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 72>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 73>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 74>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 75>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 76>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 77>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 78>
                    <?=putPiece('white_pawn', 'black_pawn');?>
                </div>
            </div>
            <div class = "row evenRow <?= getColor("even", $flip);?>">
                <div class = "cell <?= cellType('dark');?>" id = 81>
                    <?php getCellLabel($flip, 81);?>
                    <?=putPiece('white_rook', 'black_rook');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 82>
                    <?php getColumnLabel($flip, 82);?>
                    <?=putPiece('white_knight', 'black_knight');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 83>
                    <?php getColumnLabel($flip, 83);?>
                    <?=putPiece('white_bishop', 'black_bishop');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 84>
                    <?php getColumnLabel($flip, 84);?>
                    <?=putPiece('white_queen', 'black_queen');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 85>
                    <?php getColumnLabel($flip, 85);?>
                    <?=putPiece('white_king', 'black_knight');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 86>
                    <?php getColumnLabel($flip, 86);?>
                    <?=putPiece('white_bishop', 'black_bishop');?>
                </div>
                <div class = "cell <?= cellType('dark');?>" id = 87>
                    <?php getColumnLabel($flip, 87);?>
                    <?=putPiece('white_knight', 'black_knight');?>
                </div>
                <div class = "cell <?= cellType('white');?>" id = 88>
                    <?php getColumnLabel($flip, 88);?>
                    <?=putPiece('white_rook', 'black_rook');?>
                </div>
            </div>
        </div>
    </body>
</html>