class Playground {
	static printMatrix() {
		return "printMatrix has been called.";
	}

	static startGame() {
		return "startGame has been called.";
	}

	static addFigures() {
		return "addFigures has been called.";
	}

	static clickMove() {
		return "clickMove has been called.";
	}

	static moveFigure() {
		return "moveFigure has been called.";
	}
}